package com.ls;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ls.model.Employee;
import com.ls.repository.EmployeeRepository;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
class SpringBootWithAngularCrudApplicationTests {

	@Autowired
	EmployeeRepository employeeRepository;
	
//	@Test
//	void contextLoads() {
//	}

	@Test
	@Order(1)
	public void testCreate() {
		Employee employee=new Employee();
//		employee.setId(118);
		employee.setName("Tester120");
		employee.setEmail("tester120@gmail.com");
		employee.setAge(70);
		employeeRepository.save(employee);
		assertNotNull(employeeRepository.findById(employee.getId()).get());
	}
	
	@Test
	@Order(2)
	public void testReadAll() {
		List<Employee> list=employeeRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}
	
	@Test
	@Order(3)
	public void testReadById() {
		Employee employee=employeeRepository.findById(132).get();
		assertEquals(70, employee.getAge());
	}
	
	@Test
	@Order(4)
	public void testUpdate() {
		Employee employee=employeeRepository.findById(132).get();
		employee.setAge(81);
		employee.setEmail("tester120@outlook.com");
		employee.setName("Tester1200");
		employeeRepository.save(employee);
		assertNotEquals(70, employeeRepository.findById(132).get().getAge());
		assertNotEquals("tester120@gmail.com", employeeRepository.findById(132).get().getEmail());
		assertNotEquals("Tester120", employeeRepository.findById(132).get().getName());
	}
	
	@Test
	@Order(5)
	public void testDelete() {
		employeeRepository.deleteById(129);
		assertThat(employeeRepository.existsById(132)).isFalse();
	}
}
