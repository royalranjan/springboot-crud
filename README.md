# SpringBoot CRUD
This is SpringBoot(backend) with Angular(frontend) CRUD project using Postgresql database. 

## Create Spring Boot Project selected Specification 
    1. Type : Maven Project
    2. Package : Jar
    3. Java Version : 11
    4. Language : Java
    5. Dependencies : Spring Web, Spring Data JPA, Postgresql
    6. Data Base : Postgresql
    7. Operating System : Windows

## Rest Api and Related Code:
Packages and files are  Needed 
# 1. Model : Employee

```

package com.ls.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "email")
	private String email;
	@Column(name = "age")
	private int age;
	
	public Employee() {
		super();
	}
	public Employee(int id, String name, String email, int age) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.age = age;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", email=" + email + ", age=" + age + "]";
	}
	
	
}


```

# 2. Repository : EmployeeRepository (for use of Data JPA with data base)

```
package com.ls.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ls.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer>{

}

```

# 3. Controller : EmployeeController

```
package com.ls.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ls.model.Employee;
import com.ls.model.exception.ResourceNotFoundException;
import com.ls.repository.EmployeeRepository;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:4200/")
public class EmployeeController {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@GetMapping("/employees")
	public @ResponseBody List<Employee> getAllEmployees(){
		return employeeRepository.findAll();
	}
	
	@PostMapping("/employees")
	public Employee creatEmployee(@RequestBody Employee employee) {
		return employeeRepository.save(employee);
	}
	
	@GetMapping("/employees/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable Integer id) {
		
		Employee employee=employeeRepository.findById(id)
				.orElseThrow(()-> new ResourceNotFoundException("Employee not exists with id "+id));
		return ResponseEntity.ok(employee);
	}
	
	@PutMapping("/employees/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable Integer id,@RequestBody Employee  employeeDetails){
		
		Employee employee=employeeRepository.findById(id)
				.orElseThrow(()-> new ResourceNotFoundException("Employee not exists with id "+id));
		
		employee.setName(employeeDetails.getName());
		employee.setEmail(employeeDetails.getEmail());
		employee.setAge(employeeDetails.getAge());
		
		Employee updatEmployee= employeeRepository.save(employee);
		return ResponseEntity.ok(updatEmployee);
	}
	
	@DeleteMapping("/employees/{id}")
	public ResponseEntity<Map<String, Boolean>> deleteEmployee(@PathVariable Integer id){
		Employee employee=employeeRepository.findById(id)
				.orElseThrow(()-> new ResourceNotFoundException("Employee not exists with id "+id));
		
		employeeRepository.delete(employee);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}
}

```

# 4. Data Base Connection in file "application.properties"
```
#spring.datasource.url=jdbc:h2:mem:testdb
spring.jpa.defer-datasource-initialization=true

spring.datasource.url=jdbc:postgresql://localhost:5433/emp
spring.datasource.username=postgres
spring.datasource.password=postgres


spring.jpa.hibernate.ddl-auto=update
spring.jpa.show-sql=true
spring.jpa.properties.hibernate.format_sql=false
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQL81Dialect

```

# How To Run on other Machine:
```
1. First open the cmd/terminal  type : git clone https://gitlab.com/royalranjan/springboot-crud.git
2. create role by typing query in postgresql : create role user_name  with createdb login password 'postgres';
3. then make some modification in application.properties file.
4. Now your project is ready to run.

```

